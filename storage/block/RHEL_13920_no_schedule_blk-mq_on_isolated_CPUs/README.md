# storage/block/RHEL_13920_no_schedule_blk-mq_on_isolated_CPUs

Storage: don't schedule blk-mq kworkers on isolated CPUs

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
