summary: Assert a non-root user process can't write to a memory mapped device.
description: |
  Assert a non-root user process can't write to a memory mapped device.
  First try to access /dev/mem. If the operation succeeds, mmap the address
  range of the device, then try a read and a write operation.
  Either of these open/read/write operations is expected to fail.
  Test inputs:
    A program, iomem-access, will run with non-root privileges and will try to
    access a memory mapped device. Input details:
    - A non-root user created as testuser.
    - A hardware device. Default is serial.
    - IO memory range [START_ADDR, END_ADDR] for the device, taken from /proc/iomem.
    - iomem-access.c, source.
    - Access test execution with su testuser -c '/tmp/iomem-access $START_ADDR $END_ADDR'
  Expected results:
    If either of these open/read/write operations to the device fails as expected,
    you should expect the following results:
    [   PASS   ] :: Command 'gcc -o /tmp/iomem-access -D_GNU_SOURCE iomem-access.c' (Expected 0, got 0)
    open /dev/mem: No such file or directory
    [   PASS   ] :: Command 'su testuser -c '/tmp/iomem-access 0088c000 0088ffff '' (Expected 1,139, got 1)
  Results location:
    output.txt
contact: Pablo Ridolfi <pridolfi@redhat.com>
test: |
    if [ -n "${FFI_QM_SCENARIO}" ]; then
        podman exec --env TEST_DIR="$(pwd)" --env BEAKERLIB_DIR="$BEAKERLIB_DIR" \
          -t qm sh -c 'cd "$TEST_DIR"; bash runtest.sh'
    else
        bash ./runtest.sh
    fi
framework: beakerlib
adjust:
 require+:
   - gcc
   - glibc-devel
extra-summary: memory/mmra/iomem-access
extra-task: memory/mmra/iomem-access
id: d6723b25-77bf-4112-9188-e03eb7491600
